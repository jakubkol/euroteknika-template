function lazyLoadInit() {

	$('noscript[data-lazy-load]').each(function(){

		var $this = $(this);
		var $img = $('<img src="">')
			.attr('data-src', $this.attr('data-src'))
			.attr('alt', $this.attr('data-alt'))
			.attr('alt', $this.attr('data-alt'))
			.css('opacity', 0);

		$this.before($img).remove();

		$img.waypoint(function(){
			$(new Image())
				.on('load',{img:this},function(e){
					$(e.data.img)
						.attr('src', $(this).attr('src'))
						.animate( { opacity: 1}, 500 )
						.removeAttr('data-src')
				})
				.attr('src', $(this).attr('data-src'));
		},{
			offset: '110%',
		});

	});

}